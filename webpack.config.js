const path = require('path');
const HtmlWebbpackPlugin = require('html-webpack-plugin');

// Webpack configurations
// set entry which can be anything and output for the build
// add regex to tell it what to look for and what to exclude
module.exports = {
    entry: './src/index.js',
    output: {
        path: path.join(__dirname, '/dist'),
        filename: 'index_bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            }
        ]
    },
    plugins: [
        new HtmlWebbpackPlugin({
            template:"./src/index.html"
        })
    ]
}